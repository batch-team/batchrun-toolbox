######################################################################
#
# A collection of Bash functions to manipulate Batch Worker Nodes
#
# In particular, some tricks to drain, reboot, enable, kill, ... WNs in bulk
#
######################################################################

#
# Drain WN, and reboot in (max) 7 days
#
function drain-and-reboot-wn ()
{ 
    if [ $# -gt 0 ]
    then

        PARAMS=""
	DAYS=7
	MESSAGE="Executed by $USER"
        while (( "$#" ))
        do
            case "$1" in
                -d|--days)
                    shift
	            DAYS=$1
	            shift
                    ;;
		-m|--message)
		    shift
		    MESSAGE=$1
		    shift
		    ;;
                *) # preserve positional arguments
                    PARAMS="$PARAMS $1"
                    shift
                    ;;
            esac
        done

        roger update --appstate draining --message "{\"JobDate\": $(date +%s), \"TargetDate\": $(( $(date +%s) + 3600 * 24 * $DAYS)), \"Reboot\": \"asap\", \"Message\": \"$MESSAGE\"}" $PARAMS
        roger show $PARAMS
    fi
}

function drain-wn ()
{
    if [ $# -gt 0 ]
    then
 
        PARAMS=""
	DAYS=7
	MESSAGE="Executed by $USER"
        while (( "$#" ))
        do
            case "$1" in
                -d|--days)
                    shift
	            DAYS=$1
	            shift
                    ;;
		-m|--message)
		    shift
		    MESSAGE=$1
                    shift
                    ;;
                *) # preserve positional arguments
                    PARAMS="$PARAMS $1"
                    shift
                    ;;
            esac
        done

        roger update --appstate draining --message "{\"JobDate\": $(date +%s), \"TargetDate\": $(( $(date +%s) + 3600 * 24 * $DAYS)), \"Message\": \"$MESSAGE\"}" --all_alarms=false $PARAMS
        roger show $PARAMS
    fi
}
function enable-wn ()
{
    if [ $# -gt 0 ]
    then
        roger update --appstate production --all_alarms=true "$@"
    fi
}

function reboot-wn ()
{ 
    if [ $# -gt 0 ]
    then
	for h in "$@"
	do
            eval $(ai-rc -s $h)
            openstack server reboot --hard $(echo $h | cut -d. -f1)
	done
    fi
}

function kill-wn ()
{ 
    if [ $# -gt 0 ]
    then
	for h in "$@"
	do
            eval $(ai-rc -s $h)
            OS_REGION_NAME= ai-kill $h
	done
    fi
}

function console-wn ()
{ 
    if [ $# -gt 0 ]
    then
        for h in "$@"
        do
            eval $(ai-rc -s $h)
            openstack console url show -f json $(echo $h | cut -d. -f1)
        done
    fi
}

#EOF

