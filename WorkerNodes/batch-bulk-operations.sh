#! /bin/sh

# Simple script to drain or power-off certain pre-defined blocks of Batch worker nodes.
#
# Relies on mco to do the heavy lifting, and on Roger/brainslug to initiate the draining

TMPFILE=$(mktemp)
trap "/bin/rm -rf $TMPFILE 2>/dev/null" EXIT

#
# Print brief usage text
#
function usage()
{
    local name=$(basename $0)
    cat <<EOF
Usage: $name --action drain    --zone <name of datacenter area> --timestamp <data and time of intervention> [--apply]
       $name --action poweroff --zone <name of datacenter area> --timestamp <data and time of intervention> [--apply]
       $name --help
EOF

}

#
# Print help text
#
function help()
{
    local name=$(basename $0)

    usage
    
    cat <<EOF

Simple script to drain or power-off certain pre-defined blocks on Batch worker nodes.

Relies on mco to do the heavy lifting, and on Roger/brainslug to initiate the draining.

Arguments:

       -a|--action      : define the action to take

                          Possible values are:
                          * drain       - ensure Worker Nodes are emptied of Condor jobs
                          * poweroff    - power the nodes off. Note: this implies that the nodes will be drained as well.

       -z|--zone        : area of the datacenter to act on.

                          Possible values are:
                          * meyrin     - all servers in Building 513
                          * vault-wcr  - servers in the Water-Cooled Racks in the Vault of Building 513
                          * pdc        - all servers in the Prevession Data Center (aka Building 775)
                          * point8     - all servers in the 2 containers at Point-8 (aka LHCb)
                          * point8-c1  - all servers in Container #1 at Point-8 (aka LHCb)
                          * point8-c5  - all servers in Container #5 at Point-8 (aka LHCb)

       r|--rack         : rack name

                          This argument can be repeated

       -t|--timestamp   : time at which to act. Format: "YYYY-MM-DD HH:mm"
                          Special value: "now"

                          This option is only relevant for the "drain" action

       --apply          : actually act. Otherwise, just print the generated action

       -h|--help        : display this help

Examples:

       $ $name --action drain --zone meyrin --timestamp "2022-10-15 09:00"
       $ $name --action poweroff --zone pdc --timestamp now
       $ $name --action poweroff --rack SP05 --rack SP06 --timestamp now

EOF

}

#
# Get node names
#
function nodenames()
{

    # Generate list of worker node names
    local names=$(eval mco find "$MCO_FILTER" 2>/dev/null | sort | xargs)

    # Store them in a temp. file
    echo $names | xargs -n1 | sort > $TMPFILE

    echo "$names"
}

#
# "Drain" action
#
function drain()
{
    COMMAND="/usr/bin/roger update --appstate draining --hostlist $TMPFILE --message \"{\\\"JobDate\\\": $JOBDATE}\""
 
    # Now act!

    if [ $APPLY -eq 1 ]
    then
        echo "[I] Executing $COMMAND"
        eval $COMMAND
    else
        echo "[I] Dryrun: would execute \"$COMMAND\""
    fi
}

#
# "Poweroff" action
#
function poweroff()
{

    # If shutdown is in the future, prepare an "at"-job

    if [ -n "$JOBDATE" ]
    then
        now=$(/usr/bin/date +%s)
        if [ $(($JOBDATE - $now)) -gt 60 ]
        then
            jobdate=$(/usr/bin/date "+%Y%m%d%H%M" --date @$JOBDATE)
            COMMAND="echo systemctl poweroff | at -t $jobdate"
        fi
    fi

    # Otherwise, poweroff now

    [ -z "$COMMAND" ] && COMMAND="systemctl poweroff"

    # Make this an "mco"-command

    COMMAND="mco shell run '$COMMAND' $MCO_FILTER ; "

    # Now act!

    if [ $APPLY -eq 1 ]
    then
        echo "[I] Executing $COMMAND"
        eval $COMMAND 
    else
        echo "[I] Dryrun: would execute \"$COMMAND\""
    fi

    # Also, update the Roger state

    COMMAND="/usr/bin/roger update --nc_alarmed=false --os_alarmed=false --app_alarmed=false --appstate draining --hostlist $TMPFILE --message \"{\\\"JobDate\\\": $JOBDATE}\""

    if [ $APPLY -eq 1 ]
    then
        echo "[I] Executing $COMMAND"
        eval $COMMAND
    else
        echo "[I] Dryrun: would execute \"$COMMAND\""
    fi

}

#
# Main starts here
#
if [ "$#" -eq 0 ]
then
    usage
    exit 1
fi

PARAMS=""
APPLY=0
while (( "$#" ))
do
    case "$1" in
        -a|--action)
            ACTION=$2
            shift
            shift
            ;;
        -t|--timestamp)
            tstamp=$2
            if [ -z "$tstamp" ]
            then
                echo "[E] Rejecting empty timestamp"
                usage
                exit 1
            fi
            JOBDATE=$(/usr/bin/date +%s --date "$tstamp" 2>/dev/null)
            if [ $? -ne 0 ]
            then
                echo "[E] Could not parse timestamp \"$tstamp\""
                usage
                exit 1
            fi
            shift
            shift
            ;;
        -z|--zone)
            ZONE=$2
            shift
            shift
            ;;
        -r|--rack)
            RACK+=($2)
            shift
            shift
            ;;
        --apply)
            APPLY=1
            shift
            ;;
        -h|--help)
            help
            exit 0
            ;;
        -*|--*) # unsupported flags
            echo "Error: Unsupported flag $1" >&2
            echo
            help
            exit 1
            ;;
        *) # preserve positional arguments
            PARAMS="$PARAMS $1"
            shift
            ;;
    esac
done

if [ -n "$PARAMS" ]
then
    echo "[E] Unexpected arguments: $PARAMS"
    usage
    exit 1
fi

#
# Check the action
#
if [ -z "$ACTION" ]
then
    echo "[E] Missing action parameter: \"[poweroff|drain]\""
    usage
    exit 1
else
    if [ "$ACTION" != "drain" -a "$ACTION" != "poweroff" ]
    then
        echo "[E] Unsupported action \"$ACTION\" specified"
        usage
        exit 1
    fi
    
fi

#
# Check the timestamp
#
if [ -z "$JOBDATE" ]
then
    echo "[E] Missing argument \"--timestamp <data and time of intervention>\"" >&2
    usage
    exit 1
fi

#
# define MCO filter for zone or rack(s)
#
MCO_FILTER="--discovery-timeout 10 --timeout 10 --no-progress --target bi -F 'hostgroup=~bi/condor/' "
if [ -n "$ZONE" ]
then
    case "$ZONE" in
	meyrin|0513)
            MCO_FILTER+="-F datacentre=meyrin"
            ;;
	vault-wcr|0513-S-0034)
            MCO_FILTER+="-F datacentre=meyrin -F landb_rackname='/SP|SQ|ST|SU/'"
            ;;
	pdc|775)
            MCO_FILTER+="-F datacentre=pdc"
            ;;
	point8|6045)
            MCO_FILTER+="-F datacentre=point8"
            ;;
	point8-c1)
            MCO_FILTER+="-F datacentre=point8 -F landb_rackname='/S1R/'"
            ;;
	point8-c5)
            MCO_FILTER+="-F datacentre=point8 -F landb_rackname='/S5R/'"
            ;;
	*)
            echo "[E] Unsupported zone \"$ZONE\" specified"
            usage
            exit 1
            ;;
    esac
elif [ -n "$RACK" ]
then
    MCO_FILTER+="-F landb_rackname='/^"
    MCO_FILTER+=$(echo ${RACK[@]} | sed 's/ /$|^/g; s/[a-z]/\U&/g')
    MCO_FILTER+="$/'"
    #echo $MCO_FILTER ;# exit;
else
    echo "[E] Missing argument \"--zone <name of datacenter area>\" or \"--rack <rack name>\""
    usage
    exit 1
fi

#
# Get the node names
#
NODENAMES=$( nodenames )
if [ -z "$NODENAMES" ]
then
    echo "[E] Could not find any nodes, exiting..." >&2
    exit 1
else
    echo "[I] Found $(echo $NODENAMES | wc -w) nodes"
fi

#
# Do the work
#
eval $ACTION

exit 0

