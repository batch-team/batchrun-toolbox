#! /bin/sh

# Simple script to execute "openstack" actions on the servers in the specified project(s).

#set -x

unset OS_REGION_NAME

#
# Print brief usage text
#
function usage()
{
    local name=$(basename $0)
    cat <<EOF
Usage: $name --action "<openstack command>" <Cloud project name(s)>
       $name --help
EOF

}
#
# Print help text
#
function help()
{
    local name=$(basename $0)

    usage
    
    cat <<EOF

Simple script to execute "openstack" actions on the servers in the specified project(s).

Arguments:

       -a|--action      : define the action to take.

                          This action is passed to the OpenStack CLI, to execute the command "openstack <action> <server names>"

       -h|--help        : display this help

Examples:

     - Reboot all servers in a give project:

         $ $name --action "server reboot" "IT-Batch - PDC Project 001 - Physical"

     - Start all servers in 6 of the PDC projects:

         $ echo {001..006} | xargs -n1 | xargs -I{} -n1 echo "\"IT-Batch - PDC Project {} - Physical\"" |xargs $name -a "server start"

EOF

}

#
# Parse the arguments
#
if [ "$#" -eq 0 ]
then
    usage
    exit 1
fi

while (( "$#" ))
do
    case "$1" in
	-a|--action)
	    ACTION="$2"
	    shift
	    shift
	    ;;
        -h|--help)
            help
            exit 0
            ;;
        *) # preserve positional arguments
            PROJECTS+=( "$1" )
            shift
            ;;
    esac
done

if [[ -z "${ACTION}" || -z "${PROJECTS[@]}" ]]
then
    usage
    exit 1
fi

#
# Now do it
#
for project in "${PROJECTS[@]}"
do
    export OS_PROJECT_NAME="$project"
    echo "Processing project $OS_PROJECT_NAME"

    SERVERNAMES=($(openstack server list -f value -c Name | sort))

    echo "Executing \"openstack ${ACTION}\" for ${#SERVERNAMES[@]} servers"
    
    echo ${SERVERNAMES[@]} | xargs -n1 echo ${ACTION} | openstack
    
done

exit 0
